import Vue from 'vue'
import VueRouter from 'vue-router'

const Index = () => import('../views/Index.vue')
const Home = () => import('../views/base/Home.vue')
const About = () => import('../views/base/About.vue')
const Reg = () => import('../components/Reg')
const Art = () => import('../views/base/Art')
const New = () => import('../views/action/New')
const Do = () => import('../views/action/Do')
const Info = () => import('../views/action/Info')
const Arts = () => import('../views/action/Arts')
const ChangeA = () => import('../views/action/changeA')
const Admin = () => import('../views/admin/Admin')
const aArts = () => import('../views/admin/Arts')
const ChangeU = () => import('../views/action/changeU')
const Msg = () => import('../views/base/Msg')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index,
    nav: true,
    children: [
      {
        path: '/',
        name: '主页',
        component: Home,
        show: true
      },
      {
        path: '/msg',
        name: '留言',
        component: Msg,
        show: true
      },
      {
        path: '/about',
        name: '关于',
        component: About,
        show: true
      },
      {
        path: '/reg',
        name: '注册',
        component: Reg,
        show: false
      },
      {
        path: '/Article',
        name: '文章',
        component: Art,
        show: false
      },
    ]
  },
  {
    path: '/do',
    name: '个人/创作中心',
    component: Do,
    nav: false,
    children: [
      {
        path: '/do/info',
        name: '个人/创作中心',
        component: Info,
      },
      {
        path: '/do/post',
        name: '投稿',
        component: New,
      },
      {
        path: '/do/article',
        name: '文章管理',
        component: Arts,
      },
      {
        path: '/do/changeA',
        name: '文章修改',
        component: ChangeA,
      },
      {
        path: '/do/changeU',
        name: '修改资料',
        component: ChangeU,
      },
    ]
  },
  {
    path: '/admin',
    name: '控制台',
    component: Admin,
    nav: false,
    children: [
      {
        path: '/admin/article',
        name: '文章管理',
        component: aArts,
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
