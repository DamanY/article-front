import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/iview.js'

import {mavonEditor} from "mavon-editor";
import "mavon-editor/dist/css/index.css";

Vue.component("mavon-editor", mavonEditor);

Vue.config.productionTip = false

// axios.defaults.baseURL = 'http://localhost:90'
axios.defaults.baseURL = 'http://49.234.239.122:90'
axios.defaults.withCredentials = true
axios.defaults.params = {
  "ackey": "nplanlan"
}

router.beforeEach((to, from, next) => {
  if (to.name){
    document.title = to.name;
  }
  if (to.path.match('/do/')){
    axios.get('/getUser').then(function (resp) {
      if (!resp.data){
        next({
          path: '/'
        })
      }
    })
  }
  if (to.path.match('/admin')){
    axios.get('/getUser').then(function (resp) {
      if (resp.data.type!=-1){
        next({
          path: '/'
        })
      }
    })
  }
  next();
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
