## Introduction
这是一个文章论坛项目，语文老师的需求；

暂时的地址：[http://49.234.239.122](http://49.234.239.122)

分为了两个仓库：
- 本仓库
- https://gitee.com/gybaoyu/Article-Community

本仓库是项目的前端部分：Vue.js + iView

后端使用了SpringBoot + MyBatis

个人博客：[https://www.damanyang.cn/](https://www.damanyang.cn/)

By Daman & Abalone

### Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run s
```

### Compiles and minifies for production
```
npm run b
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).